#  Copyright (c) 2018 - Present  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

APP := scalingApp
APPDB := $(APP)/Db
APPSRC := $(APP)/src

LIB_SYS_LIBS += gsl gslcblas

HEADERS += $(APPSRC)/scalingBase.h
HEADERS += $(APPSRC)/scalingPoly.h
HEADERS += $(APPSRC)/scalingSqrt.h
HEADERS += $(APPSRC)/loadcsv.h
HEADERS += $(APPSRC)/lutTools.h

SOURCES += $(APPSRC)/scalingPoly.cpp
SOURCES += $(APPSRC)/scalingSqrt.cpp
SOURCES += $(APPSRC)/loadcsv.cpp
SOURCES += $(APPSRC)/lutTools.cpp

USR_DBFLAGS += -I $(APPDB)

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

SUBS = $(wildcard $(APPDB)/*.substitutions)
TEMPLATES += $(wildcard $(APPDB)/*.template)

.PHONY: vlibs
vlibs:
